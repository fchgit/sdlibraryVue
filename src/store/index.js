import {
	createStore
} from 'vuex'

export default createStore({
	state: {
		item: 0,
		imageUrl: "http://localhost:8081/Library/img/",
		ifFrameShow: true
	},
	mutations: {
		changItem(state, item) {
			state.item = item
		}
	},
	actions: {},
	modules: {}
})
