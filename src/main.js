import {
	createApp
} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'

import axios from 'axios'

//设置axios默认Url地址前缀
axios.defaults.baseURL = "http://localhost:8081/Library/"

// http response 响应拦截器
axios.interceptors.request.use(config => {
	//登录成功后将后台返回的TOKEN在本地存下来,每次请求从sessionStorage中拿到存储的TOKEN值 
	let token = localStorage.getItem('STORAGE_TOKEN');
	if (token) {
		//如果请求时TOKEN存在,就为每次请求的headers中设置好TOKEN,后台根据headers中的TOKEN判断是否放行 
		//config.headers['token'] = localStorage.token;
		config.headers.token = token
	}
	return config;
}, error => {
	// 对响应错误做点什么
	return Promise.reject(error);
});

// http response 响应拦截器
axios.interceptors.response.use(response => {
	// 在接收响应做些什么，例如跳转到登录页
	if (response.data.code == 401) {
		router.push({
			path: 'login'
		})
	}
	let token = response.headers.token;
	if (token) {
		//如果请求时TOKEN存在,就为每次请求的headers中设置好TOKEN,后台根据headers中的TOKEN判断是否放行 
		localStorage.setItem('STORAGE_TOKEN', token)
	}
	return response;
}, error => {
	// 对响应错误做点什么
	if (error.response.status === 403) {
		alert("权限不足")
		router.push({
			path: '/'
		})
	}
	return Promise.reject(error);
});


const app = createApp(App)
app.use(store).use(router).mount('#app')
