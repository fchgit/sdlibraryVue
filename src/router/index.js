import {
	createRouter,
	createWebHistory
} from 'vue-router'
import Book from '../views/Book'
import Login from '../views/Login'
import Main from '../views/Main'
import MyInfo from '../views/MyInfo'
import Search from '../views/Search'
import Register from '../views/Register'
import Admin from '../views/Admin/Admin'
import AddBook from '../views/Admin/AddBook'
import AddBookClass from '../views/Admin/AddBookClass'
import UpdateBook from '../views/Admin/UpdateBook'
import AdminSearch from '../views/Admin/AdminSearch'
import History from '../views/Admin/History'
import Test from '../views/Test'
const routes = [{
		path: '/',
		name: 'Main',
		component: Main
	},
	{
		path: '/Book',
		name: 'Book',
		component: Book
	},
	{
		path: '/Test',
		name: 'Test',
		component: Test
	},
	{
		path: '/MyInfo',
		name: 'MyInfo',
		component: MyInfo
	},
	{
		path: '/Login',
		name: 'Login',
		component: Login
	},
	{
		path: '/Search',
		name: 'Search',
		component: Search
	},
	{
		path: '/Register',
		name: 'Register',
		component: Register
	},
	{
		path: '/Admin',
		name: 'Admin',
		component: Admin
	},
	{
		path: '/Admin/AddBook',
		name: 'AddBook',
		component: AddBook
	},
	{
		path: '/Admin/AddBookClass',
		name: 'AddBookClass',
		component: AddBookClass
	},
	{
		path: '/Admin/UpdateBook',
		name: 'UpdateBook',
		component: UpdateBook
	},
	{
		path: '/Admin/AdminSearch',
		name: 'AdminSearch',
		component: AdminSearch
	},
	{
		path: '/Admin/History',
		name: 'History',
		component: History
	}
]

const router = createRouter({
	linkActiveClass: 'active',
	history: createWebHistory(process.env.BASE_URL),
	routes
})

export default router
