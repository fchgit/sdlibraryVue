# 图书管理Vue前端

## 主要内容：

应用所学知识，完成一个Web应用程序的开发，Android端，前端Html、CSS、JavaScript、BootStrap、Vue等框架，后端用的SpringBoot等开发技术来设计一个小型的系统。

后端: [图书管理后端: 后端主要技术：Springboot+Mybatis (gitee.com)](https://gitee.com/fchgit/sdlibrarySpring)

Android: [图书管理系统Android端: 图书管理系统的Android客户端 (gitee.com)](https://gitee.com/fchgit/sdlibrary)

#### 主要技术：

Vue，Axios

#### 目录结构

```
.
│  App.vue
│  list.txt
│  main.js
│  
├─assets
│      logo.png
│      R-C.jpg
│      
├─components
│      Book.vue
│      Head.vue
│      Side.vue
│      
├─router
│      index.js
│      
├─store
│      index.js
│      
└─views
    │  Book.vue
    │  Login.vue
    │  Main.vue
    │  MyInfo.vue
    │  Register.vue
    │  Search.vue
    │  Test.vue
    │  
    └─Admin
            AddBook.vue
            AddBookClass.vue
            Admin.vue
            AdminSearch.vue
            History.vue
            UpdateBook.vue
            
```

### 项目部署

1. 下载[Node.js 中文网 (nodejs.cn)](http://nodejs.cn/)并安装
2. 在项目根目录下(与src文件同级目录)打开cmd命令行
3. 在命令行输入` npm install`等待依赖下载
4. 下载完毕后输入`npm run serve`等待项目启动, 启动默认端口为8080



### 用例说明

U-用户（User）

A-管理员（Admin）

| 编号 | 用例编号 | 用例名称     | 参与者 |
| ---- | -------- | ------------ | ------ |
| 1    | US-01    | 注册账户     | U      |
| 2    | US-02    | 登录账户     | U A    |
| 3    | US-03    | 搜索图书     | U A    |
| 4    | US-04    | 查看图书详情 | U A    |
| 5    | US-05    | 查看个人信息 | U A    |
| 6    | US-06    | 借阅图书     | U A    |
| 7    | US-07    | 归还图书     | U A    |
| 8    | US-08    | 添加图书     | A      |
| 9    | US-09    | 修改图书     | A      |
| 10   | US-10    | 添加分类     | A      |
| 11   | US-11    | 查看借阅记录 | A      |

 

#### 注册账户

| 用例编号 | US-01                                                        |
| -------- | ------------------------------------------------------------ |
| 用例名称 | 注册账户                                                     |
| 用例描述 | 参与者在注册界面注册属于自己的账户，从而使用系统             |
| 参与者   | 用户                                                         |
| 前置条件 | 无                                                           |
| 后置条件 | 参与者注册成功，用户id回传并跳转到登录界面                   |
| 基本路径 | 1. 参与者启动系统<br />2.参与者跳转到http://localhost:8080/Register<br />3.参与者输入个人详细信息<br />4.系统注册成功，返回到登录界面 |
| 扩展点   | 检查手机和邮箱是否重复                                       |

 

#### 登录系统

| 用例编号 | US-02                                                        |
| -------- | ------------------------------------------------------------ |
| 用例名称 | 登录系统                                                     |
| 用例描述 | 系统验证用户身份合法后进入系统                               |
| 参与者   | 用户，管理员                                                 |
| 前置条件 | 参与者成功注册账户或拥有账号                                 |
| 后置条件 | 用户，管理员登录成功，跳转到首页服务器返回token              |
| 基本路径 | 1.参与者启动系统<br />2.参与者跳转到http://localhost:8080/Login<br />3.参与者输入用户id<br />4.参与者输入密码<br />5.参与者提交登录信息<br />6.系统检测信息完整性及用户身份合法性<br />7.系统返回登录结果 |
| 扩展点   | 可使用id、邮箱或手机号登录                                   |

 

#### 搜索图书

| 用例编号 | US-03                                                        |
| -------- | ------------------------------------------------------------ |
| 用例名称 | 搜索图书                                                     |
| 用例描述 | 参与者登录后可使用搜索功能搜索图书                           |
| 参与者   | 用户，管理员                                                 |
| 前置条件 | 参与者成功登录或token未过期                                  |
| 后置条件 | 系统返回搜索结果                                             |
| 基本路径 | 1.参与者启动系统<br />2.参与者登录系统<br />3.参与者跳转到http://localhost:8080/Search<br />4.参与者输入图书分类（可选），图书名（可选）或图书（ISBN）<br />5.参与者提交搜索信息   6.系统返回搜索结果 |
| 扩展点   | 无                                                           |

 

#### 查看图书详情

| 用例编号 | US-04                                                        |
| -------- | ------------------------------------------------------------ |
| 用例名称 | 查看图书详情                                                 |
| 用例描述 | 参与者查看图书详情                                           |
| 参与者   | 用户，管理员                                                 |
| 前置条件 | 参与者成功登录或token未过期                                  |
| 后置条件 | 系统返回图书详情                                             |
| 基本路径 | 1.参与者启动系统<br />2.参与者登录<br />3.参与者跳转到<http://localhost:8080/Book?id=>（图书id）<br />4.系统返回图书详情 |
| 扩展点   | 无                                                           |

 

#### 查看个人信息

| 用例编号 | US-05                                                        |
| -------- | ------------------------------------------------------------ |
| 用例名称 | 查看个人信息                                                 |
| 用例描述 | 参与者查看个人详情                                           |
| 参与者   | 用户，管理员                                                 |
| 前置条件 | 参与者成功登录或token未过期                                  |
| 后置条件 | 系统返回当前用户信息                                         |
| 基本路径 | 1.参与者启动系统<br />2.参与者登录<br />3.参与者跳转到<http://localhost:8080/MyInfo><br />4.系统返回当前借阅的图书信息 |
| 扩展点   | 显示更多个人信息                                             |

 

#### 借阅图书

| 用例编号 | US-06                                                        |
| -------- | ------------------------------------------------------------ |
| 用例名称 | 借阅图书                                                     |
| 用例描述 | 参与者借阅图书                                               |
| 参与者   | 用户，管理员                                                 |
| 前置条件 | 参与者成功登录或token未过期并且当前没有借书                  |
| 后置条件 | 系统返回借阅结果                                             |
| 基本路径 | 1.参与者启动系统<br />2.参与者登录<br />3.参与者跳转到任意图书详情页或点击别处出现的对应图书的借阅按钮<br />4.系统返回借阅结果 |
| 扩展点   | 显示更多个人信息                                             |

 

#### 归还图书

| 用例编号 | US-07                                                        |
| -------- | ------------------------------------------------------------ |
| 用例名称 | 归还图书                                                     |
| 用例描述 | 参与者归还图书                                               |
| 参与者   | 用户，管理员                                                 |
| 前置条件 | 参与者成功登录或token未过期并且有借阅的图书                  |
| 后置条件 | 系统返回还书结果                                             |
| 基本路径 | 1.参与者启动系统<br />2.参与者登录<br />3.参与者跳转到个人信息页<br />4.参与者点击还书按钮<br />5.系统返回还书结果 |
| 扩展点   | 无                                                           |

 

#### 添加图书

| 用例编号 | US-08                                                        |
| -------- | ------------------------------------------------------------ |
| 用例名称 | 添加图书                                                     |
| 用例描述 | 参与者添加图书                                               |
| 参与者   | 管理员                                                       |
| 前置条件 | 参与者成功登录或token未过期并且角色为admin，添加信息完整     |
| 后置条件 | 系统返回添加结果                                             |
| 基本路径 | 1.参与者启动系统<br />2.参与者登录<br />3.参与者跳转到http://localhost:8080/Admin<br />4.参与者点击添加图书跳转到Admin/AddBook<br />5.参与者添加完整信息后点击提交按钮<br />6.系统返回添加结果 |
| 扩展点   | 检查ISBN是否重复                                             |

 

#### 修改图书

| 用例编号 | US-09                                                        |
| -------- | ------------------------------------------------------------ |
| 用例名称 | 修改图书                                                     |
| 用例描述 | 参与者修改图书                                               |
| 参与者   | 管理员                                                       |
| 前置条件 | 参与者成功登录或token未过期并且角色为admin，修改信息完整     |
| 后置条件 | 系统返回修改结果                                             |
| 基本路径 | 1.参与者启动系统<br />2.参与者登录<br />3.参与者跳转到http://localhost:8080/Admin<br />4.参与者点击修改图书跳转到Admin/AdminSearch<br />5.参与者搜索到图书后点击修改按钮跳转到指定图书的修改页面Admin/UpdateBook?id=（图书id）<br />6.系统返回图书原本数据 <br />7.参与者输入修改数据<br />8.参与者点击提交修改按钮<br />9.系统返回修改结果 |
| 扩展点   | 检查ISBN是否重复                                             |

 

#### 添加图书分类

| 用例编号 | US-10                                                        |
| -------- | ------------------------------------------------------------ |
| 用例名称 | 添加图书分类                                                 |
| 用例描述 | 参与者添加图书分类                                           |
| 参与者   | 管理员                                                       |
| 前置条件 | 参与者成功登录或token未过期并且角色为admin，添加信息完整     |
| 后置条件 | 系统返回添加结果                                             |
| 基本路径 | 1.参与者启动系统<br />2.参与者登录<br />3.参与者跳转到http://localhost:8080/Admin<br />4.参与者点击添加图书分类跳转到Admin/AddBookClass<br />5.参与者输入一级分类、二级分类（可选）、三级分类（可选）<br />6.参与者点击添加一级分类或二级分类或三级分类 <br />7.系统返回添加结果 |
| 扩展点   | 检查分类是否重复                                             |

 

#### 查看借阅记录

| 用例编号 | US-11                                                        |
| -------- | ------------------------------------------------------------ |
| 用例名称 | 查看借阅记录                                                 |
| 用例描述 | 参与者查看所有用户借阅记录                                   |
| 参与者   | 管理员                                                       |
| 前置条件 | 参与者成功登录或token未过期并且角色为admin                   |
| 后置条件 | 系统返回添加结果                                             |
| 基本路径 | 1.参与者启动系统 <br />2.参与者登录 <br />3.参与者跳转到http://localhost:8080/Admin<br />4.参与者点击查看借阅历史跳转到Admin/History<br />5.系统返回查询结果 |
| 扩展点   | 添加搜索功能                                                 |



## 运行效果截图

### 普通用户相关界面

登录

![1653886468731](https://gitee.com/fchgit/sdlibraryVue/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/1653886468731.png)

首页

![1653886578168](https://gitee.com/fchgit/sdlibraryVue/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/1653886578168.png)

搜索界面

![1653886632677](https://gitee.com/fchgit/sdlibraryVue/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/1653886632677.png)

图书详情页

![1653886695322](https://gitee.com/fchgit/sdlibraryVue/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/1653886695322.png)

### 管理员相关界面

管理员主页

![1653886987380](https://gitee.com/fchgit/sdlibraryVue/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/1653886987380.png)

添加图书

![1653887047090](https://gitee.com/fchgit/sdlibraryVue/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/1653887047090.png)

添加图书分类

![1653887076448](https://gitee.com/fchgit/sdlibraryVue/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/1653887076448.png)

修改图书

![1653887122667](https://gitee.com/fchgit/sdlibraryVue/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/1653887122667.png)

查询借阅历史

![1653887157554](https://gitee.com/fchgit/sdlibraryVue/raw/master/%E6%95%88%E6%9E%9C%E5%9B%BE/1653887157554.png)



